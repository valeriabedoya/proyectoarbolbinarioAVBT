class Nodo:
    def __init__(self, valor):
        self.valor = valor
        self.izquierda = None
        self.derecha = None


class ArbolBinario:
    def __init__(self):
        self.raiz = None

    def insertar(self, elemento):
        if self.raiz is None:
            self.raiz = Nodo(elemento)
        else:
            self.insertar_aux(self.raiz, elemento)

    def insertar_aux(self, nodo, valor):
        if valor <= nodo.valor:
            if nodo.izquierda is None:
                nodo.izquierda = Nodo(valor)
            else:
                self.insertar_aux(nodo.izquierda, valor)
        else:
            if nodo.derecha is None:
                nodo.derecha = Nodo(valor)
            else:
                self.insertar_aux(nodo.derecha, valor)

    def eliminar(self, elemento):
        pass

    def existe(self, elemento):
        return self.existe_aux(self.raiz, elemento)

    def existe_aux(self, nodo, valor):
        if nodo is None:
            return False
        elif valor == nodo.valor:
            return True
        elif valor < nodo.valor:
            return self.existe_aux(nodo.izquierda, valor)
        else:
            return self.existe_aux(nodo.derecha, valor)

    def maximo(self, nodo=None):
        if nodo is None:
            nodo = self.raiz

        while nodo.derecha is not None:
            nodo = nodo.derecha
        return nodo.valor

    def minimo(self, nodo=None):
        if nodo is None:
            nodo = self.raiz

        while nodo.izquierda is not None:
            nodo = nodo.izquierda
        return nodo.valor

    def altura(self):
        return self.altura_aux(self.raiz)

    def altura_aux(self, nodo):
        if nodo is None:
            return 0
        else:
            altura_izq = self.altura_aux(nodo.izquierda)
            altura_der = self.altura_aux(nodo.derecha)
            return max(altura_izq, altura_der) + 1

    def obtener_elementos(self, nodo=None):
        if nodo is None:
            nodo = self.raiz

        elementos = []
        stack = []

        while stack or nodo:
            if nodo:
                stack.append(nodo)
                nodo = nodo.izquierda
            else:
                nodo = stack.pop()
                elementos.append(nodo.valor)
                nodo = nodo.derecha

        return elementos
    def __str__(self):
        return str(self.obtener_elementos(self.raiz))